server '40.68.170.161', user: 'deploy', roles: %w(app db web)
set :branch, 'master'
set :ssh_options, forward_agent: true


set :linked_files, fetch(:linked_files, []).push(
    'config/database.yml', 'config/secrets.yml'
)
set :linked_dirs, fetch(:linked_dirs, []).push(
    'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets',
    'vendor/bundle', 'public/system'
)