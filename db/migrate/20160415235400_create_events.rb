class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|

      t.belongs_to :user
      t.belongs_to :garden

      t.integer :event_type
      t.text :message

      t.timestamps null: false
    end
  end
end
