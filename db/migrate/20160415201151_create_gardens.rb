class CreateGardens < ActiveRecord::Migration
  def change
    create_table :gardens do |t|
      t.float :latitude
      t.float :longitude
      t.text :description
      t.string :name
      t.integer :status
    end
  end
end
