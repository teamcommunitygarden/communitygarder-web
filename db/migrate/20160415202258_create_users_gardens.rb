class CreateUsersGardens < ActiveRecord::Migration
  def change
    create_table :users_gardens do |t|
      t.belongs_to :user
      t.belongs_to :garden
    end
  end
end
