class Add < ActiveRecord::Migration
  def change
    add_column :photos, :user_id, :integer
    add_column :photos, :garden_id, :integer
  end
end
