class RemoveAdminFromGarden < ActiveRecord::Migration
  def change
    add_column :users_gardens, :admin, :boolean, :default =>  false
  end
end
