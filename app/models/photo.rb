class Photo < ActiveRecord::Base

  belongs_to :garden
  belongs_to :user

  has_attached_file :photo, styles: { medium: '300x300>', thumb: '100x100>' }, default_url: ''
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
end
