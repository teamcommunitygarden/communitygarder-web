class Garden < ActiveRecord::Base
  has_many :users_gardens
  has_many :users, through: :users_gardens
  has_many :events
  has_many :photos


  def owner
    owner = users_gardens.where(:admin => true).first.user
    {id: owner.id, username: owner.username}
  end

  def distance
    rand(2.00..10.0)
  end

end
