module Api
  module V1
    class GardensController < ApiController
      before_filter :authenticate_user


      def index
        gardens = Garden.all
        render :json => gardens.to_json(:include => {:users => {:only => [:id, :username]}})
      end

      def show
        garden = Garden.find(params[:id])
        photos = [
            "http://www.canadiangardening.com/img/photos/biz/CanadianGardening/garden-ste-annes-LG.jpg",
            "http://www.landscapegardener.info/communities/9/004/012/503/909//images/4606632869.jpg",
            "http://www.optimumgarden.com/wp-content/uploads/2014/10/Controlling-Garden-Pests-How-to-Take-Back-Your-Garden.jpg",
            "http://www.canadiangardening.com/img/photos/biz/CanadianGardening/garden-ste-annes-LG.jpg",
            "http://www.landscapegardener.info/communities/9/004/012/503/909//images/4606632869.jpg",
            "http://www.optimumgarden.com/wp-content/uploads/2014/10/Controlling-Garden-Pests-How-to-Take-Back-Your-Garden.jpg"
        ]
        result = {garden: JSON.parse(garden.to_json(:include => {:users => {:only => [:id, :username]}})), message_feed: garden.events, recent_photos: photos}
        render :json => result
      end


      def create
        garden = Garden.create(prepare_params)
        UsersGarden.create(:user => current_user, :admin => true, :garden => garden)

        render :json => garden.to_json(:include => {:users => {:only => [:id, :username]}}, :methods => [:owner, :distance],)
      end


      def join
        garden = Garden.find(params[:garden_id])
        current_user.gardens << garden
        Event.create(:type => 0, :garden_id => garden.id, :user_id => current_user.id)
      end

      def photos
        result = current_user.photos.map{|p| "#{request.protocol}#{request.host_with_port}#{p.photo.url}"}
        puts result
        render json: result
      end

      def upload_photo
        photo = Photo.new(photo: params[:image])
        photo.garden_id = current_user.gardens.first.id
        photo.user_id = current_user.id
        if photo.save
          # garden =

          # current_user.photos << photo
          render json: { photo: "#{request.protocol}#{request.host_with_port}#{photo.photo.url}"}, status: :ok
        else
          render json: { message: current_user.errors.messages, code: 422 }, status: :unprocessable_entity
        end
      end

      protected

      def prepare_params
        {
          name: params[:name],
          description: params[:description],
          status: 1,
          latitude: params[:latitude],
          longitude: params[:longitude],
          address: params[:address]
        }
      end

      def photo_params
        params.permit(:photo)
      end

    end
  end
end
