module Api
  module V1
    class EventsController < ApiController

      def create
        event = current_user.events.create(prepare_params)

        render :json => event
      end

      def index
        render :json => Event.where(:garden_id => current_user.gardens)
      end

      protected

      def prepare_params
        {
          event_type: params[:type],
          message: params[:message],
          garden_id: params[:garden_id]
        }
      end
    end
  end
end
