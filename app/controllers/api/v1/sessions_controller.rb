module Api
  module V1
    class SessionsController < Devise::SessionsController
      skip_before_filter :verify_authenticity_token, :only => [:create]
      respond_to :json
      before_filter :ensure_params_exist

      def create
        resource = User.find_for_database_authentication(:email=>params[:session][:email])
        return invalid_login_attempt unless resource

        if resource.valid_password?(params[:session][:password])
          sign_in("user", resource)
          render :json=> {:authentication_token=>resource.authentication_token, :id=>resource.id, :email => resource.email, :username=> resource.username}
          return
        end
        invalid_login_attempt
      end

      def destroy
        sign_out(resource_name)
      end

      protected
      def ensure_params_exist
        return unless params[:session].blank?
        render :json=>{:success=>false, :message=>"missing user_login parameter"}, :status=>422
      end

      def invalid_login_attempt
        warden.custom_failure!
        render :json=> {:success=>false, :message=>"Error with your login or password"}, :status=>401
      end
    end
  end
end

